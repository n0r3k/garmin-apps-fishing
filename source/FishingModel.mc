using Toybox.ActivityRecording as AR; 
using Toybox.FitContributor as Fit;
using Toybox.WatchUi as Ui;
using Toybox.System;
using Toybox.Attention;
using Toybox.Timer;
using Toybox.Position;

class FishingModel
{
//    hidden var _timer;
	hidden var _graphTimer;
	hidden var _timerIsRunning = false;
    hidden var _fishCount;
    hidden var _timerFishCount;
    
    hidden var _session;
    hidden var _currentFishCounterField;
    hidden var _totalFishCounterField;

//	var _seconds;

    function initialize() {
//		_seconds = 0;
        _fishCount = 0;

        _session = AR.createSession({:sport=>AR.SPORT_FISHING, :name=>"Fishing"});
        _currentFishCounterField = _session.createField("Fish", 1, Fit.DATA_TYPE_UINT8, {:mesgType => Fit.MESG_TYPE_SESSION});
        _currentFishCounterField.setData(0);

		if ( SettingsModel.getProperty( "useGPS" ) ) {
	        _totalFishCounterField = _session.createField("Fish location", 2, Fit.DATA_TYPE_UINT8, {:mesgType => Fit.MESG_TYPE_RECORD});
	        _totalFishCounterField.setData(0);
		}
    }

    function start() {
//        _timer = new Timer.Timer();
//        _timer.start(method(:onTimer), 1000, true);

        if ( _session == null ) {
        	initialize();
        }
        
        _session.start();
    }

    function stop() {
//        _timer.stop();
        _session.stop();
    }

    function save() {
        _session.save();
		_session = null;
		_fishCount = 0;
    }

    function discard() {
        _session.discard();
        _session = null;
		_fishCount = 0;
    }

	function addFish() {
		_fishCount += 1;
        _currentFishCounterField.setData( _fishCount );
        
		if ( SettingsModel.getProperty( "useGPS" ) ) {
			if ( _timerIsRunning ) {
				_graphTimer.stop();
			}

			_graphTimer = new Timer.Timer();
	        _graphTimer.start(method(:onGraphTimer), 10000, false);
	        _timerIsRunning = true;
			if ( _timerFishCount == null ) {
				_timerFishCount = _fishCount - 1;
     		}
        }
	}

	function subtractFish() {
		if ( _fishCount > 0 ) {
			_fishCount -= 1;

			if ( SettingsModel.getProperty( "useGPS" ) ) {        	
				if ( _timerFishCount == _fishCount ) {
					_graphTimer.stop();
					_timerIsRunning = false;
				}
			}
		}
	}

	function onGraphTimer() {
		if ( SettingsModel.getProperty( "useGPS" ) ) {        
        	_totalFishCounterField.setData( _fishCount );		
			_timerFishCount = null;
		}
	}

    function getCurrentFishCount() {
        return _fishCount;
    }

//    function onTimer() {
//    	if ( SettingsModel.getProperty("useGPS") == true ) {
//			_seconds++;
//			
//	    	if ( _seconds == 60 ) {
//	    	    Position.enableLocationEvents(Position.LOCATION_ONE_SHOT, method(:onPosition));    	
//				_seconds = 0;
//	    	}    
//		}        
//    }
    
    function onPosition(info) {
    }
}