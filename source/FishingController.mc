using Toybox.Timer;
using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.System;
using Toybox.Attention;

class FishingController
{
    var _timer;
    var _model;
	var _beerCounterModel;
    var _running;

    function initialize() {
        _timer = null;
        _model = App.getApp().model;
        _running = false;
    }

    function start() {
	    if (Attention has :playTone) {
	    	Attention.playTone( Attention.TONE_START );
		}
		
		if (Attention has :vibrate) {			
	    	Attention.vibrate([ new Attention.VibeProfile(40, 700) ]);
    	}
    	
        _model.start();
        _running = true;
    }

    function stop() {
	    if (Attention has :playTone) {
	    	Attention.playTone( Attention.TONE_STOP );
		}
		
		if (Attention has :vibrate) {			
	    	Attention.vibrate([ new Attention.VibeProfile(40, 700) ]);
    	}

        _model.stop();
        _running = false;
    }

    function save() {
        _model.save();
        Ui.pushView(new Ui.ProgressBar("Saving...", null), new FishingProgressDelegate(), WatchUi.SLIDE_DOWN);
        _timer = new Timer.Timer();
        _timer.start(method(:onExit), 3000, false);
    }

    function discard() {
        _model.discard();
        Ui.pushView(new Ui.ProgressBar("Discarding...", null), new FishingProgressDelegate(), WatchUi.SLIDE_DOWN);
        _timer = new Timer.Timer();
        _timer.start(method(:onExit), 3000, false);
    }

    function isRunning() {
        return _running;
    }

    function onStartStop() {
        if(_running) {
            stop();
            Ui.pushView(new Rez.Menus.MainMenu(), new FishingMenuDelegate(), WatchUi.SLIDE_UP);
        } else {
            start();
        }
    }

    // Handle timing out after exit
    function onExit() {
    	var _beerCounterModel = App.getApp().beerCounterModel;

    	if ( _beerCounterModel.getPromille().toFloat() > 0 )
    	{
    		Ui.switchToView( new BeerCounterView(), new BeerCounterDelegate(), Ui.SLIDE_IMMEDIATE );
    	}
    	else
    	{
	        System.exit();
		}
    }
    
    function addFish() {
    	if ( _running ) {
    		_model.addFish();
    	}
    }

	function subtractFish() {
		if ( _running ) {
			_model.subtractFish();		
		}
	}
}