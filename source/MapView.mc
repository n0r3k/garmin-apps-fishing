using Toybox.WatchUi as Ui;
using Toybox.Application as App;
using Toybox.Position;
using Toybox.System;
using Toybox.Time;
using Toybox.Graphics as Gfx;

class MapView extends Ui.MapTrackView {
	
    function initialize() {
        MapTrackView.initialize();

		setMapMode(Ui.MAP_MODE_BROWSE);

        var top_left = new Position.Location({:latitude => 56.965928, :longitude =>24.168337, :format => :degrees});
        var bottom_right = new Position.Location({:latitude => 56.965928, :longitude =>24.168337, :format => :degrees});

        MapTrackView.setMapVisibleArea(top_left, bottom_right);
        
        MapTrackView.setScreenVisibleArea(0, System.getDeviceSettings().screenHeight / 2, System.getDeviceSettings().screenWidth, System.getDeviceSettings().screenHeight);
    }
    
    function onUpdate(dc) {
        MapTrackView.onUpdate(dc);

		// TIME		
		var currentTime = Time.Gregorian.info( Time.now(), Time.FORMAT_SHORT );		
		
		var currentTimeString = Lang.format(
		    "$1$:$2$",
		    [
		        currentTime.hour.format("%02d"),
		        currentTime.min.format("%02d")
		    ]
		);

		new Ui.Text({   	
            :text=> currentTimeString,
            :color=>Gfx.COLOR_BLUE,
            :font=>Gfx.FONT_XTINY,
            :locX => ( dc.getWidth() / 2 ) - ( dc.getTextWidthInPixels( currentTimeString, Gfx.FONT_XTINY ) / 2 ),
            :locY=> Ui.LAYOUT_VALIGN_TOP
        }).draw(dc);
	}
}