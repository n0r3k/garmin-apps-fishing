using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Position;

class FishingApp extends App.AppBase {

    var model;
    var controller;
	var beerCounterModel;
	var beerCounterController;
	var mapModel;
	var mapController;
	var _deviceType;

    function initialize() {
        AppBase.initialize();
        model = new $.FishingModel();
        controller = new $.FishingController();
		beerCounterModel = new $.BeerCounterModel();
		beerCounterController = new $.BeerCounterController();
		mapModel = new $.MapModel();
		mapController = new $.MapController();
    }

    // onStart() is called on application start up
    function onStart(state) {
    	_deviceType = Ui.loadResource( Rez.Strings.deviceType ).toString();
    	if ( SettingsModel.getProperty( "useGPS" ) ) {
	        Position.enableLocationEvents(Position.LOCATION_CONTINUOUS, method(:onPosition));
    	}
    }

	function onSettingsChanged() {
        Position.enableLocationEvents( SettingsModel.getProperty( "useGPS" ) ? Position.LOCATION_CONTINUOUS : Position.LOCATION_DISABLE, method(:onPosition));
	}

    // onStop() is called when your application is exiting
    function onStop(state) {
    	if ( SettingsModel.getProperty( "useGPS" ) ) {
	        Position.enableLocationEvents(Position.LOCATION_DISABLE, method(:onPosition));
		}
    }

    function onPosition(info) {
    }
    
    // Return the initial view of your application here
    function getInitialView() {
        return [ new FishingView(), new FishingDelegate() ];
    }
}