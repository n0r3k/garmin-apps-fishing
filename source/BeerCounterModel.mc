using Toybox.ActivityRecording as AR; 
using Toybox.FitContributor as Fit;
using Toybox.WatchUi as Ui;
using Toybox.System;
using Toybox.Attention;
using Toybox.Time;
using Toybox.UserProfile as User;

class BeerCounterModel
{
	const R_GENDER_FEMALE = 0.55;
	const R_GENDER_MALE = 0.68;

//    hidden var _timer;
    hidden var _beerCount;
//    hidden var _totalBeerCount;
	
	hidden var _genderRatio;
	hidden var _userWeight;
	
	hidden var _currentPromille;
	hidden var _promillePerBeer;
	hidden var _endTime;
	hidden var _durationPerBeer;
	
//    hidden var _currentBeerCounterField;
//    hidden var _totalBeerCounterField;

    function initialize() {				
        _beerCount = 0;
		_currentPromille = 0.00;
		_endTime = new Time.Moment( 0 );
		
		// check if we have access to person data gender / weight
		if ( User has :getProfile ) {
			var profile = User.getProfile();
			_genderRatio = profile.gender == profile.GENDER_MALE ? R_GENDER_MALE : R_GENDER_FEMALE;
			_userWeight = profile.weight;
		}
		else
		{
			_genderRatio = ( R_GENDER_FEMALE + R_GENDER_MALE ) / 2;
			_userWeight = 70000;
		}
	
//		_genderRatio = R_GENDER_MALE;
//		_userWeight = 78000;


		//		5 fluid oz of wine
		//		Alcohol % varies. If alcohol content is 12.0% by volume:
		//		5 x 29.5735 x 0.12 x .789 ≈ 14 grams of alcohol
		var _gramsOfAlcohol = SettingsModel.getProperty( "setBeverageVolume" ).toNumber() * ( SettingsModel.getProperty( "setBeverageAlcoholContent" ).toFloat() / 100 ) * 0.789;	// TODO : IMPLEMENT IN SETTINGS IN FUTURE

		// we can easily calculate promiles and time per 1 beer for current person on init		
		var _rawNumber = _gramsOfAlcohol / ( _userWeight * _genderRatio );

		_promillePerBeer = _rawNumber * 1000 * SettingsModel.getProperty( "setAlcoholAbsorbtionRate" ).toFloat();
		_durationPerBeer = new Time.Duration( _promillePerBeer / 0.16 * 60 * 60 );

//        _totalBeerCount = 0;

//        _session = AR.createSession({:sport=>AR.SPORT_GENERIC, :name=>"Fishing"});
//        _currentBeerCounterField = _session.createField("Beer", 3, Fit.DATA_TYPE_UINT8, {:mesgType => Fit.MESG_TYPE_SESSION});
//        _currentBeerCounterField.setData(0);
//        _totalFishCounterField = _session.createField("Total", 2, Fit.DATA_TYPE_UINT32, {:mesgType => Fit.MESG_TYPE_SESSION});
//        _totalFishCounterField.setData(0);
    }

	function addBeer() {
		_beerCount += 1;		
				
		if ( _currentPromille == 0 ) {
			_endTime = _endTime.add( new Time.Duration( Time.now().value() ) );
		}
				
		_currentPromille += _promillePerBeer;
		_endTime = _endTime.add( _durationPerBeer );		
	}

	function subtractBeer() {
		if ( _beerCount > 0 ) {
			_beerCount -= 1;
			_currentPromille -= _promillePerBeer;
			
			if ( _currentPromille < 0 ) {
				_currentPromille = 0;
				_endTime = new Time.Moment( 0 );
			}
			else
			{						
				_endTime = _endTime.subtract( _durationPerBeer );		
			}
		}
	}

    function getCurrentBeerCount() {
        return _beerCount.toNumber();
    }

	function getPromille() {
		return _currentPromille.format("%.2f");
	}

	function getPromilleTimer() {
		if ( _currentPromille > 0 ) {
			var today;
			var timeNow = new Time.Duration( Time.now().value() );
			
			// SDK 2+
			if ( Time.Gregorian has :utcInfo ) {
				today = Time.Gregorian.utcInfo( _endTime.subtract( timeNow ), Time.FORMAT_SHORT);
			}
			// SDK 1
			else
			{
				var clockTime = System.getClockTime();
				
				today = Time.Gregorian.info( 
					_endTime.subtract( timeNow )
							.subtract( new Time.Duration( clockTime.timeZoneOffset - clockTime.dst ) )
				, Time.FORMAT_SHORT);			
			}
								
			// fix for over 24h period
			var days = ( _endTime.subtract( timeNow ).value() / 86400 ).toNumber() * 24;
			var dateString = Lang.format(
			    "$1$:$2$:$3$",
			    [
			        ( today.hour + days ).format("%02d"),
			        today.min.format("%02d"),
			        today.sec.format("%02d")
			    ]
			);
			
			if ( today.hour + days == 0 && today.min == 0 && today.sec == 0 && Attention has :vibrate) {
		    	Attention.vibrate([ new Attention.VibeProfile(40, 200) ]);
		    	Attention.vibrate([ new Attention.VibeProfile(0, 50) ]);
		    	Attention.vibrate([ new Attention.VibeProfile(40, 200) ]);
		    	Attention.vibrate([ new Attention.VibeProfile(0, 50) ]);
		    	Attention.vibrate([ new Attention.VibeProfile(40, 200) ]);
			}
			
			// need to update promilles in reverse
			_currentPromille = ( ( today.hour + days ) + ( today.min.toFloat() / 60 ) + ( today.sec.toFloat() / 60 / 60 ) )  * 0.16; 

			return dateString.toString();				
		}	
		else
		{
			_currentPromille = 0;
			_endTime = new Time.Moment( 0 );
			return "00:00:00";
		}
	}
}