using Toybox.WatchUi as Ui;
using Toybox.System as Sys;

class FishingQuitMenuDelegate extends Ui.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    // Handle the menu input
    function onMenuItem(item) {
		if (item == :settings) {
        	Ui.pushView( new Rez.Menus.SettingsMenu(), new FishingSettingsMenuDelegate(), Ui.SLIDE_LEFT );
        } else if (item == :goToBeer) {
        	Ui.switchToView( new BeerCounterView(), new BeerCounterDelegate(), Ui.SLIDE_IMMEDIATE );	        
        } else if (item == :quit) {	
        	Sys.exit();
        } else if (item == :resume) {
        	return true;
        } else {
            System.exit();
            return true;
        }
        return false;
    }
}