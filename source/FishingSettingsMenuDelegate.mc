using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class FishingSettingsMenuDelegate extends Ui.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    // Handle the menu input
    function onMenuItem(item) {
        if (item == :menu_gps) {
	        Ui.pushView(new Rez.Menus.setGPS(), new FishingSettingsMenuDelegate(), Ui.SLIDE_IMMEDIATE);
        } else if ( item == :menu_gps_on ) {
 			SettingsModel.setProperty("useGPS", true);
        } else if ( item == :menu_gps_off ) {
 			SettingsModel.setProperty("useGPS", false);               
        }
        
        return false;
    }
}