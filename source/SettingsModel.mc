using Toybox.Application as App;


class SettingsModel {

	function setProperty(key, value) {
		if ( App has :Storage ) {
			App.Properties.setValue(key, value);
		} else {
			App.AppBase.setProperty(key, value);
		}

		return true;
	}

	function getProperty(key) {
		if ( App has :Storage ) {
			return App.Properties.getValue(key);
		} else {
			return App.AppBase.getProperty(key);
		}
	}

}