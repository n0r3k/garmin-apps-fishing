using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class FishingDelegate extends Ui.BehaviorDelegate {

    var _controller;
    
    function initialize() {
        BehaviorDelegate.initialize();
        _controller = App.getApp().controller;
    }

	function onKey( keyEvent ) {
		if ( keyEvent.getKey() == Ui.KEY_DOWN ) {
			_controller.subtractFish();					
		}
		else if ( keyEvent.getKey() == Ui.KEY_UP ) {
			_controller.addFish();		    
		}
//		else if ( keyEvent.getKey() == Ui.KEY_ESC ) {
//	        Ui.pushView(new Rez.Menus.QuitMenu(), new FishingQuitMenuDelegate(), Ui.SLIDE_UP); 		
//		}
	}

	function onSelect() {
        _controller.onStartStop();	
	}

    function onBack() {
		if ( !_controller.isRunning() ) {	        
	        Ui.pushView(new Rez.Menus.QuitMenu(), new FishingQuitMenuDelegate(), Ui.SLIDE_UP); 
		} else {
    		Ui.switchToView( new BeerCounterView(), new BeerCounterDelegate(), Ui.SLIDE_LEFT );								
		}
		return true;
    }
    
    function onSwipe(swipeEvent) {
		if ( swipeEvent.getDirection().equals( Ui.SWIPE_UP ) )
		{
			_controller.addFish();
    	}    

		if ( swipeEvent.getDirection().equals( Ui.SWIPE_DOWN ) )
		{
			_controller.subtractFish();
    	}    

		if ( swipeEvent.getDirection().equals( Ui.SWIPE_LEFT ) ) {
    		Ui.switchToView( new BeerCounterView(), new BeerCounterDelegate(), Ui.SLIDE_LEFT );					
		}
	
		// exit from app allowed only when activity not running
		if ( swipeEvent.getDirection().equals( Ui.SWIPE_RIGHT ) && !_controller.isRunning() ) 
		{
	        Ui.pushView(new Rez.Menus.QuitMenu(), new FishingQuitMenuDelegate(), WatchUi.SLIDE_RIGHT);
		}

        return true;
    }
	
    function onMenu() {
		return true;
    }
}