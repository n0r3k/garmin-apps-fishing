using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;
using Toybox.Activity;
using Toybox.Time;
using Toybox.Timer;
using Toybox.Application as App;
using Toybox.Sensor;
using Toybox.System;

class FishingView extends Ui.View {

	var _timer;
	var _heartRate;
	var _model;
	var _clockTime;
	var _deviceType;
	
    function initialize() {
        View.initialize();

		// Enable HR sensor
	    Sensor.setEnabledSensors([Sensor.SENSOR_HEARTRATE]);
		Sensor.enableSensorEvents(method(:onSensor));
		
		_model = App.getApp().model;	
        _timer = new Timer.Timer();        
		_clockTime = System.getClockTime();
		_deviceType = App.getApp()._deviceType;
    }

    function onLayout(dc) {
		setLayout(Rez.Layouts.MainLayout(dc)); 		
    }

    function onShow() {
        _timer.start(method(:onTimer), 1000, true);    
    }

    // Update the view
    function onUpdate(dc) {    
                
        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);

//        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_ORANGE);
//    	dc.drawLine(120, 0, 120, 240);
//        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_DK_GRAY);
//    	dc.drawLine(0, 120, 240, 120);
		
		var locY = 25;
					
		if ( _deviceType.equals( "fenix3" ) || _deviceType.equals( "fr230" ) || _deviceType.equals( "semiround" ) ) {
			locY = 15;		
		}
				
		var fishCountText = new Ui.Text({
            :text=>_model.getCurrentFishCount().toString(),
            :color=>Gfx.COLOR_WHITE,
            :font=>Gfx.FONT_NUMBER_HOT,
            :locX => 115,
            :locY=> locY
        });
        fishCountText.setJustification(Gfx.TEXT_JUSTIFY_RIGHT);
        fishCountText.draw(dc);		

		// Activity Timer
		var activityTimer = 0;
		var activityInfo = Activity.getActivityInfo();
				
		if ( activityInfo.timerTime != null ) {					
			activityTimer = activityInfo.timerTime / 1000;
	    }
		
		var today;

		if ( Time.Gregorian has :utcInfo ) {
			today = Time.Gregorian.utcInfo( new Time.Moment( activityTimer ), Time.FORMAT_SHORT);
		}
		else
		{
			today = Time.Gregorian.info( new Time.Moment( activityTimer ).subtract( new Time.Duration( _clockTime.timeZoneOffset - _clockTime.dst ) ), Time.FORMAT_SHORT);		
		}
		
		var dateString = Lang.format(
		    "$1$:$2$:$3$",
		    [
		        today.hour.format("%02d"),
		        today.min.format("%02d"),
		        today.sec.format("%02d")
		    ]
		);

		var font = Gfx.FONT_NUMBER_MILD;
		
		if ( _deviceType.equals( "fenix3" ) || _deviceType.equals( "fr230" ) || _deviceType.equals( "semiround" ) ) {
			font = Gfx.FONT_LARGE;
		}

		new Ui.Text({   	
            :text=> dateString,
            :color=>Gfx.COLOR_WHITE,
            :font=> font,
            :locX => ( dc.getWidth() / 2 ) - ( dc.getTextWidthInPixels( dateString, font ) / 2 ),
            :locY=>Ui.LAYOUT_VALIGN_CENTER
        }).draw(dc);
        
		// TIME		
		var currentTime = Time.Gregorian.info( Time.now(), Time.FORMAT_SHORT );		
		
		var currentTimeString = Lang.format(
		    "$1$:$2$",
		    [
		        currentTime.hour.format("%02d"),
		        currentTime.min.format("%02d")
		    ]
		);

		new Ui.Text({   	
            :text=> currentTimeString,
            :color=>Gfx.COLOR_BLUE,
            :font=>Gfx.FONT_XTINY,
            :locX => ( dc.getWidth() / 2 ) - ( dc.getTextWidthInPixels( currentTimeString, Gfx.FONT_XTINY ) / 2 ),
            :locY=> Ui.LAYOUT_VALIGN_TOP
        }).draw(dc);

		// Heart Rate	
		locY = dc.getHeight() - 55;
		
		if ( _deviceType.equals( "fenix3" ) ) {
			locY = dc.getHeight() - 70;
		} else if ( _deviceType.equals( "fr230" ) || _deviceType.equals( "semiround" ) ) {
			locY = dc.getHeight() - 45;			
		} else if ( _deviceType.equals( "fenix6" ) || _deviceType.equals( "fenix6pro" ) || _deviceType.equals( "vivoactive4" ) || _deviceType.equals("fenix7") || _deviceType.equals("fenix7x")) {
			locY = dc.getHeight() - 90;			
		}
		
		var hrText = new Ui.Text({   	
            :text=> _heartRate == null ? "--" : _heartRate.toString(),
            :color=>Gfx.COLOR_WHITE,
            :font=>Gfx.FONT_NUMBER_MEDIUM,
            :locX => dc.getWidth() / 2 - 10,
            :locY=> locY
        });        
        hrText.setJustification(Gfx.TEXT_JUSTIFY_LEFT);
        hrText.draw(dc);        				
    }

	function onSensor(sensorInfo) {
		if ( sensorInfo has :heartRate ) {			
			_heartRate = sensorInfo.heartRate;			
		}
	}

    function onHide() {
        _timer.stop();
    }
    
    function onTimer() {
        Ui.requestUpdate();
    }
        
//    function setPosition(info) {
//        posnInfo = info;
//        Ui.requestUpdate();
//    }
}
