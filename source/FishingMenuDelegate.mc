using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class FishingMenuDelegate extends Ui.MenuInputDelegate {

    hidden var _controller;
    hidden var _deathTimer;

    function initialize() {
        MenuInputDelegate.initialize();
        _controller = App.getApp().controller;
    }

    // Handle the menu input
    function onMenuItem(item) {
        if (item == :resume) {
            _controller.start();
            return true;
        } else if (item == :save) {
            _controller.save();
            return true;
        } else {
            _controller.discard();
            return true;
        }
        return false;
    }
}