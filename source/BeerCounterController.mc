using Toybox.Application as App;

class BeerCounterController
{
    var _model;

    function initialize() {
        _model = Application.getApp().beerCounterModel;
    }
    
    function addBeer() {
		_model.addBeer();
    }

	function subtractBeer() {
		_model.subtractBeer();		
	}
}