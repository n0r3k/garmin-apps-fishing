using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;
using Toybox.Time;
using Toybox.Timer;
using Toybox.Application as App;
using Toybox.System;

class BeerCounterView extends Ui.View {

	var _timer;
	var _model;
	var _deviceType;
	
    function initialize() {
        View.initialize();

		_model = App.getApp().beerCounterModel;	
        _timer = new Timer.Timer();        
		_deviceType = App.getApp()._deviceType;
    }

    function onLayout(dc) {
		setLayout(Rez.Layouts.BeerCounterLayout(dc));     
    }

    function onShow() {
        _timer.start(method(:onTimer), 1000, true);    
    }

    function onUpdate(dc) {    
        View.onUpdate(dc);

		var locY = 25;
					
		if ( _deviceType.equals( "fenix3" ) || _deviceType.equals( "fr230" ) || _deviceType.equals( "semiround" ) ) {
			locY = 15;		
		}

		var beerCountText = new Ui.Text({
            :text=>_model.getCurrentBeerCount().toString(),
            :color=>Gfx.COLOR_WHITE,
            :font=>Gfx.FONT_NUMBER_HOT,
            :locX => 115,
            :locY=> locY
        });
        beerCountText.setJustification(Gfx.TEXT_JUSTIFY_RIGHT);
        beerCountText.draw(dc);		
		
		// Promille Timer
		var promilleTimer = _model.getPromilleTimer();
		
		var font = Gfx.FONT_NUMBER_MILD;
		
		if ( _deviceType.equals( "fenix3" ) || _deviceType.equals( "fr230" ) || _deviceType.equals( "semiround" ) ) {
			font = Gfx.FONT_LARGE;
		}
				
		new Ui.Text({   	
            :text=> promilleTimer,
            :color=>Gfx.COLOR_WHITE,
            :font=> font,
            :locX => ( dc.getWidth() / 2 ) - ( dc.getTextWidthInPixels( promilleTimer, font ) / 2 ),
            :locY=>Ui.LAYOUT_VALIGN_CENTER
        }).draw(dc);		

        
		// TIME		
		var currentTime = Time.Gregorian.info( Time.now(), Time.FORMAT_SHORT );		
		
		var currentTimeString = Lang.format(
		    "$1$:$2$",
		    [
		        currentTime.hour.format("%02d"),
		        currentTime.min.format("%02d")
		    ]
		);

		new Ui.Text({   	
            :text=> currentTimeString,
            :color=>Gfx.COLOR_BLUE,
            :font=>Gfx.FONT_XTINY,
            :locX => ( dc.getWidth() / 2 ) - ( dc.getTextWidthInPixels( currentTimeString, Gfx.FONT_XTINY ) / 2 ),
            :locY=> Ui.LAYOUT_VALIGN_TOP
        }).draw(dc);
        
		// BAC / Promille
		var promille = _model.getPromille();
		var promilleColor = Gfx.COLOR_WHITE;
		
		if ( promille.toFloat() > 0 && promille.toFloat() < 0.5 ) {
			promilleColor = Gfx.COLOR_YELLOW;
		} else if ( promille.toFloat() >= 0.5 ) {
			promilleColor = Gfx.COLOR_RED;
		}

		locY = dc.getHeight() - 55;
		
		if ( _deviceType.equals( "fenix3" ) ) {
			locY = dc.getHeight() - 70;
		} else if ( _deviceType.equals( "fr230" ) || _deviceType.equals( "semiround" ) ) {
			locY = dc.getHeight() - 45;			
		} else if ( _deviceType.equals( "fenix6" ) || _deviceType.equals( "fenix6pro" ) || _deviceType.equals( "vivoactive4" ) || _deviceType.equals("fenix7") || _deviceType.equals("fenix7x")) {
			locY = dc.getHeight() - 90;			
		}
		   		   
    	var promileText = new Ui.Text({   	
            :text=> promille.toString(),
            :color=> promilleColor,
            :font=>Gfx.FONT_NUMBER_MEDIUM,
            :locX => dc.getWidth() / 2 - 10,
            :locY=> locY
        });        

        promileText.setJustification(Gfx.TEXT_JUSTIFY_LEFT);
        promileText.draw(dc);
    }

    function onHide() {
        _timer.stop();
    }
    
    function onTimer() {
        Ui.requestUpdate();
    }    
}
