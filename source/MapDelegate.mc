using Toybox.WatchUi as Ui;
using Toybox.Application as App;
using Toybox.System as Sys;

class MapDelegate extends Ui.BehaviorDelegate {

    var _controller;
    var _fishingController;
    
    function initialize() {
        BehaviorDelegate.initialize();
        _controller = App.getApp().mapController;
        _fishingController = App.getApp().controller;
    }

    function onKey(keyEvent) {
		if ( keyEvent.getKey().equals( Ui.KEY_UP ) )
		{
//			_controller.addBeer();		
		}

		if ( keyEvent.getKey().equals( Ui.KEY_DOWN ) )
		{
//			_controller.subtractBeer();		
		}

        return true;
    }
    
	function onSelect() {
        _fishingController.onStartStop();
	}

    function onBack() {
		if ( !_fishingController.isRunning() ) {
	        Ui.pushView(new Rez.Menus.QuitMenu(), new FishingQuitMenuDelegate(), WatchUi.SLIDE_UP);
		} else {
    		Ui.switchToView( new FishingView(), new FishingDelegate(), Ui.SLIDE_LEFT );								
		}
		return true;
    }    
    
    function onSwipe(swipeEvent) {
		if ( swipeEvent.getDirection().equals( Ui.SWIPE_UP ) )
		{
//			_controller.addBeer();
    	}    

		if ( swipeEvent.getDirection().equals( Ui.SWIPE_DOWN ) )
		{
//			_controller.subtractBeer();
    	}    

		if ( swipeEvent.getDirection().equals( Ui.SWIPE_RIGHT ) || swipeEvent.getDirection().equals( Ui.SWIPE_LEFT ) ) 
		{
			Ui.switchToView( new FishingView(), new FishingDelegate(), WatchUi.SLIDE_RIGHT);
		}

        return true;
    }
	
    function onMenu() {
		return true;
    }
}