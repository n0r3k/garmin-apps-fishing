using Toybox.WatchUi as Ui;
using Toybox.Application as App;
using Toybox.System as Sys;

class BeerCounterDelegate extends Ui.BehaviorDelegate {

    var _controller;
    var _fishingController;
    var _mapController;
    
    function initialize() {
        BehaviorDelegate.initialize();
        _controller = App.getApp().beerCounterController;
        _fishingController = App.getApp().controller;
        _mapController = App.getApp().mapController;
    }

    function onKey(keyEvent) {
		if ( keyEvent.getKey().equals( Ui.KEY_UP ) )
		{
			_controller.addBeer();		
		}

		if ( keyEvent.getKey().equals( Ui.KEY_DOWN ) )
		{
			_controller.subtractBeer();		
		}

        return true;
    }
    
	function onSelect() {
        _fishingController.onStartStop();
	}

    function onBack() {
		if ( !_fishingController.isRunning() ) {
	        Ui.pushView(new Rez.Menus.QuitMenu(), new FishingQuitMenuDelegate(), WatchUi.SLIDE_UP);
		} else {
			// check for Map support
			if (Ui has :MapTrackView && SettingsModel.getProperty( "useGPS" )) {		
    			Ui.switchToView( new MapView(), new MapDelegate(), Ui.SLIDE_LEFT );								
			} else {
    			Ui.switchToView( new FishingView(), new FishingDelegate(), Ui.SLIDE_LEFT );								
			}			
		}
		return true;
    }    
    
    function onSwipe(swipeEvent) {
		if ( swipeEvent.getDirection().equals( Ui.SWIPE_UP ) )
		{
			_controller.addBeer();
    	}    

		if ( swipeEvent.getDirection().equals( Ui.SWIPE_DOWN ) )
		{
			_controller.subtractBeer();
    	}    

		if ( swipeEvent.getDirection().equals( Ui.SWIPE_RIGHT ) || swipeEvent.getDirection().equals( Ui.SWIPE_LEFT ) ) 
		{
			if (Ui has :MapTrackView && SettingsModel.getProperty( "useGPS" )) {		
    			Ui.switchToView( new MapView(), new MapDelegate(), Ui.SLIDE_RIGHT );								
			} else {
    			Ui.switchToView( new FishingView(), new FishingDelegate(), Ui.SLIDE_RIGHT );								
			}			
		}

        return true;
    }
	
    function onMenu() {
		return true;
    }
}